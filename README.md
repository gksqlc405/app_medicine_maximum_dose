### 식품의약품안전처_의약품 성분별 1일 최대투여량 정보 APP
###### 공공데이터 오픈 API 사용
![Covid](./images/medicine1.png)
***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 의약품 성분별 1일 최대투여량 정보
  - 최대 투여량 보여주기
  - 투여경로 보여주기
  - 투여 단 보여주기

```


# app 화면

### 의약품 리스트 전체
![Medicine](./images/medicine2.png)

### 의약품 상세검색
![Medicine](./images/medicine3.png)

