import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';

class ComponentCustomLoading extends StatefulWidget {
  const ComponentCustomLoading({super.key, required this.cancelFunc});

  final CancelFunc cancelFunc;   // 닫을때 필요해서 필수

  @override
  State<ComponentCustomLoading> createState() => _ComponentCustomLoadingState();
}

class _ComponentCustomLoadingState extends State<ComponentCustomLoading> with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
      duration: const Duration(
        milliseconds: 1000,
      ),
      vsync: this,
    );

    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {  // dismissed = 창이꺼젔을때
        animationController.forward();   // forward = 첨으로 되돌린다
      }
    });
    animationController.forward();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 16,
          bottom: 16,
          left: 28,
          right: 28,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FadeTransition(
              opacity: animationController,
              child: Image.asset(
                'assets/태연.png',
                width: 100,
              ),
            ),
            SizedBox(height: 10,),
            Text(
                'Loading...',
              style: TextStyle(
                fontFamily: 'maple',
              ),

            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
