
import 'package:app_medicine_maximum_dose/model/drug_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final DrugListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.pink),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.DRUG_CPNT_KOR_NM == null ? '성분명(한글)이 없습니다' : item.DRUG_CPNT_KOR_NM}',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 18,
              ),
            ),
            Text('${item.DRUG_CPNT_ENG_NM == null ? '성분명(영어)가 없습니다.' : item.DRUG_CPNT_ENG_NM}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.DAY_MAX_DOSG_QY == null ? '1일 최대 투여량이 없습니다.' : item.DAY_MAX_DOSG_QY}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.DAY_MAX_DOSG_QY_UNIT == null ? '투여단위가 없습니다.' : item.DAY_MAX_DOSG_QY_UNIT}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.DOSAGE_ROUTE_CODE == null ? '투여경로가 없습니다.' : item.DOSAGE_ROUTE_CODE}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.FOML_NM == null ? '제형명이 없습니다.' : item.FOML_NM}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.CPNT_CD == null ? '성분코드가 없습니다.' : item.CPNT_CD}',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.FOML_CD == null ? '제형코드가 없습니다.' : item.FOML_CD}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

