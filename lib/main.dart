import 'package:app_medicine_maximum_dose/pages/page_index.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'drug Demo',
      localizationsDelegates: const [             // 앱전체에 들어가는에 이기 때문에 메인에다가 추가한다
        GlobalMaterialLocalizations.delegate,    //즉 대행자이다 하나의 특정 객체가 모든일을 처리하는것이아니 처리해야할 일 중 일부 다른 객체에게 넘기 것
        GlobalWidgetsLocalizations.delegate,     // 데이터 전달, 상호통신, 이벤트리스터
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const PageIndex(),
    );
  }
}

