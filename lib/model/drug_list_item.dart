class DrugListItem {
  String CPNT_CD;
  String DRUG_CPNT_KOR_NM;
  String DRUG_CPNT_ENG_NM;
  String FOML_CD;
  String FOML_NM;
  String DOSAGE_ROUTE_CODE;
  String DAY_MAX_DOSG_QY;
  String DAY_MAX_DOSG_QY_UNIT;


  DrugListItem(
    this.CPNT_CD,
    this.DRUG_CPNT_KOR_NM,
    this.DRUG_CPNT_ENG_NM,
    this.FOML_CD,
    this.FOML_NM,
    this.DOSAGE_ROUTE_CODE,
    this.DAY_MAX_DOSG_QY,
    this.DAY_MAX_DOSG_QY_UNIT
);

  factory DrugListItem.fromJson(Map<String, dynamic> json) {
    return DrugListItem(
      json['CPNT_CD'] == null ? '' : json['CPNT_CD'],    // 초기값을 애초에 주었
      json['DRUG_CPNT_KOR_NM'] == null ? '' : json['DRUG_CPNT_KOR_NM'],   // null 값이면 '' 빈값을 주고 아니면 값을 주겠다
      json['DRUG_CPNT_ENG_NM'] == null ? '' : json['DRUG_CPNT_ENG_NM'],
      json['FOML_CD'] == null ? '' : json['FOML_CD'],
      json['FOML_NM'] == null ? '' : json['FOML_NM'],
      json['DOSAGE_ROUTE_CODE'] == null ? '' : json['DOSAGE_ROUTE_CODE'],
      json['DAY_MAX_DOSG_QY'] == null ? '' : json['DAY_MAX_DOSG_QY'],
      json['DAY_MAX_DOSG_QY_UNIT'] == null ? '' : json['DAY_MAX_DOSG_QY_UNIT'],

    );
  }
}