
import 'package:app_medicine_maximum_dose/model/drug_list.dart';

class DrugResponse {
  DrugList? body;

  DrugResponse({
    this.body
});

  factory DrugResponse.fromJson(Map<String, dynamic> json) {
    return DrugResponse(
      body: json['body'] == null ? null : DrugList.fromJson(json['body']),
    );
  }
}