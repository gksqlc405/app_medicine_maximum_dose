
import 'package:app_medicine_maximum_dose/components/component_appbar_filter.dart';
import 'package:app_medicine_maximum_dose/components/component_count_title.dart';
import 'package:app_medicine_maximum_dose/components/component_custom_loading.dart';
import 'package:app_medicine_maximum_dose/components/component_list_item.dart';
import 'package:app_medicine_maximum_dose/components/components_no_contents.dart';
import 'package:app_medicine_maximum_dose/model/drug_list_item.dart';
import 'package:app_medicine_maximum_dose/pages/page_search.dart';
import 'package:app_medicine_maximum_dose/repository/repo_drug_list.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

// AutomaticKeepAliveClientMixin : 한번 뿌렸던 화면 정보 저장.
class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();

  List<DrugListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _CPNT_CD = '';
  String _DRUG_CPNT_KOR_NM = '';
  String _DRUG_CPNT_ENG_NM = '';

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    if (_currentPage <= _totalPage) {

      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {    // 시작하기전에 미리 띄우고
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });


      await RepoDrugList()
          .getList(page: _currentPage, CPNT_CD: _CPNT_CD, DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM, DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)
          .then((res)  {
        BotToast.closeAllLoading();   // 성공했을때도 닫고

        setState(() {
          _totalPage = res.totalPage;
          _totalItemCount = res.totalItemCount;

          _list = [..._list, ...res.items];

          _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading());     // 실패했을때도 닫어라

      }

    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '의약품 정보',
        actionIcon: Icons.search,

        callback: () async {
          final searchResult = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PageSearch(
                    CPNT_CD: _CPNT_CD,
                    DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
                    DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM,
                  )
              )
          );

          if (searchResult != null && searchResult[0]) {
            _CPNT_CD = searchResult[1];
            _DRUG_CPNT_KOR_NM = searchResult[2];
            _DRUG_CPNT_ENG_NM = searchResult[3];
            _loadItems(reFresh: true);
          }
        },
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          SizedBox(
            height: 30,
          ),
          ComponentCountTitle(icon: Icons.access_alarm_rounded, count: _totalItemCount, unitName: '건', itemName: '의약품 정보'),
          SizedBox(
            height: 30,
          ),
          _buildBody(),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListItem(item: _list[index], callback: () {}),
          )
        ],
      );
    } else {
      return SizedBox(
        // 앱바 높이가 40으로 고정되었으므로 정확한 수치 40을 이제 알 수 있음.
        height: MediaQuery.of(context).size.height - 40 - 20,
        child: const ComponentNoContents(icon: Icons.ac_unit_sharp, msg: '의약품 정보가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}
