import 'package:app_medicine_maximum_dose/components/component_appbar_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageSearch extends StatefulWidget {
   PageSearch({super.key, this.CPNT_CD = '', this.DRUG_CPNT_KOR_NM = '', this.DRUG_CPNT_ENG_NM = ''});

  String CPNT_CD;
  String DRUG_CPNT_KOR_NM;
  String DRUG_CPNT_ENG_NM;

  @override
  State<PageSearch> createState() => _PageSearchState();
}

class _PageSearchState extends State<PageSearch> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();  // 사용할 글로벌키 만들기

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
          title: '의약품 상세 검색',

      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: OutlinedButton(
              onPressed: () {
                String inputCPNT_CD = _formKey.currentState!.fields['CPNT_CD']!.value;
                String inputDRUG_CPNT_KOR_NM = _formKey.currentState!.fields['DRUG_CPNT_KOR_NM']!.value;
                String inputDRUG_CPNT_ENG_NM = _formKey.currentState!.fields['DRUG_CPNT_ENG_NM']!.value;

                Navigator.pop(
                    context,
                    [true, inputCPNT_CD, inputDRUG_CPNT_KOR_NM, inputDRUG_CPNT_ENG_NM],
                );
              },
              child: const Text('검색하기'),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        SizedBox(
          height: 20,
        ),
        FormBuilder(
          key: _formKey, // 글로벌키
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FormBuilderTextField(
                name: 'CPNT_CD',
                initialValue: widget.CPNT_CD,
                decoration: const InputDecoration(
                  suffixIcon: Text(
                    'CODE',
                    style: TextStyle(
                      height: 3,
                      fontFamily: 'maple',
                      fontSize: 10,
                      color: Colors.grey,
                    ),
                  ),
                  labelText: '성분코드',
                  border: OutlineInputBorder(
                  ),
                  icon: Icon(Icons.perm_identity),

                ),
              ),
              SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'DRUG_CPNT_KOR_NM',
                initialValue: widget.DRUG_CPNT_KOR_NM,
                decoration: const InputDecoration(
                    suffixIcon: Text(
                      '가나다',
                      style: TextStyle(
                        height: 3,
                        fontFamily: 'maple',
                        fontSize: 10,
                        color: Colors.grey,
                      ),
                    ),
                    labelText: '성분명(한글)',
                  border: OutlineInputBorder(),
                  icon: Icon(Icons.perm_identity),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'DRUG_CPNT_ENG_NM',
                initialValue: widget.DRUG_CPNT_ENG_NM,
                decoration: const InputDecoration(
                    suffixIcon: Text(
                      'ABC',
                      style: TextStyle(
                        height: 3,
                        fontFamily: 'maple',
                        fontSize: 10,
                        color: Colors.grey,
                      ),
                    ),
                    labelText: '성분명(영문)',
                  border: OutlineInputBorder(),
                  icon: Icon(Icons.perm_identity),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
