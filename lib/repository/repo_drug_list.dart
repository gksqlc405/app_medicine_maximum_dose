

import 'package:app_medicine_maximum_dose/configs/config_api.dart';
import 'package:app_medicine_maximum_dose/model/drug_list.dart';
import 'package:app_medicine_maximum_dose/model/drug_response.dart';
import 'package:dio/dio.dart';

class RepoDrugList {
  final String _baseUrl = '$apiDrug/1471000/DayMaxDosgQyByIngdService/getDayMaxDosgQyByIngdInq';

  Future<DrugList> getList({int page = 1, String CPNT_CD = '', String DRUG_CPNT_KOR_NM = '', String DRUG_CPNT_ENG_NM = ''}) async {
    Map<String, dynamic> params = {}; // <<제이슨
    params['pageNo'] = page;
    params['serviceKey'] = Uri.encodeFull(apiDrugKey);
    params['type'] = 'json';
    if (CPNT_CD != '') params['CPNT_CD'] = Uri.encodeFull(CPNT_CD);
    if (DRUG_CPNT_KOR_NM != '') params['DRUG_CPNT_KOR_NM'] = Uri.encodeFull(DRUG_CPNT_KOR_NM);
    if (DRUG_CPNT_ENG_NM != '') params['DRUG_CPNT_ENG_NM'] = Uri.encodeFull(DRUG_CPNT_ENG_NM);

    Dio dio = Dio();

    final response = await dio.get(     // 말그대로 get 에 주소를 넣었다
      _baseUrl,  // 윗주소
      queryParameters: params,  // 서비스키와 타입
      options: Options(
        followRedirects: false,
      )
    );

    DrugResponse resultModel = DrugResponse.fromJson(response.data);
    return resultModel.body!;
  }
}